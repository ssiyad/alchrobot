import requests
import json

from bs4 import BeautifulSoup


WIKI_BASE_URL = "https://chatwars-wiki.de"
ALCHEMY = "/index.php?title=Alchemy"
RESOURCES = "/index.php?title=Alchemy_Resource"


class Timer:
    def __init__(self):
        self.total = 0

    def inc(self, res: requests.Response):
        self.total += res.elapsed.seconds

    def show(self, res: requests.Response):
        self.inc(res)
        print(res.url, "took", res.elapsed.seconds, "seconds")

    def show_total(self):
        print("completed in", self.total, "seconds")


def scrap_recipes():
    timer = Timer()
    response = requests.get(WIKI_BASE_URL + ALCHEMY)
    timer.show(response)
    scrapman = BeautifulSoup(response.text, features="html.parser")
    _d = None
    with open("data.json", "r") as f:
        _d = json.load(f)
    for row in scrapman.findAll("tr")[:33]:
        _id = row.find("td", class_="ID")
        _name = row.find("td", class_="Item")
        if _id:
            response = requests.get(WIKI_BASE_URL + _name.a["href"])
            timer.show(response)
            item_page = BeautifulSoup(response.text, features="html.parser")
            table_right_rows = (
                item_page.find("table", class_="wikitable floatright")
            ).findAll("tr")
            _recipe = {}
            _props = {}
            _mana = table_right_rows[3].findAll("td")[1].text.strip("\n").strip(" ")
            for resource in table_right_rows[7:]:
                _recipe[resource.findAll("td")[0].text.strip("\n")] = resource.findAll(
                    "td"
                )[1].text.strip("\n")
            table_main_rows = (
                item_page.findAll("table", class_="wikitable")[1]
            ).findAll("tr")
            for prop in table_main_rows[3:]:
                _props[prop.findAll("td")[0].text.strip("\n")] = prop.findAll("td")[
                    1
                ].text.strip("\n")
            if _id.text in _d:
                _d[_id.text]["recipe"] = _recipe
            else:
                _d[_id.text] = {
                    "id": _id.text,
                    "name": _name.text,
                    "mana": _mana,
                    "recipe": _recipe,
                    **_props,
                }
    with open("data.json", "w") as f:
        json.dump(_d, f, indent=4)
    timer.show_total()


def scrap_resources():
    timer = Timer()
    response = requests.get(WIKI_BASE_URL + RESOURCES)
    timer.show(response)
    scrapman = BeautifulSoup(response.text, features="html.parser")
    _d = {}
    for herb in scrapman.find("table").findAll("tr")[1:]:
        _id = herb.find("td", class_="ID")
        _name = herb.find("td", class_="Name")
        response = requests.get(WIKI_BASE_URL + _name.a["href"])
        timer.show(response)
        item_page = BeautifulSoup(response.text, features="html.parser")
        table_right = item_page.find("table", class_="wikitable floatright")
        _props = {}
        for prop in item_page.select_one("table[class='wikitable']").findAll("tr")[3:]:
            if "Drop" in prop.findAll("td")[0].text:
                break
            _props[prop.findAll("td")[0].text.strip("\n")] = prop.findAll("td")[
                1
            ].text.strip("\n")
        _d[_id.text] = {
            "id": _id.text,
            "name": _name.text,
            **_props,
        }
        table_right = item_page.find("table", class_="wikitable floatright")
        if table_right:
            _d[_id.text]["mana"] = (
                table_right.findAll("tr")[3]
                .findAll("td")[1]
                .text.strip("\n")
                .strip(" ")
            )
    with open("data.json", "w") as f:
        json.dump(_d, f, indent=4)
    timer.show_total()


if __name__ == "__main__":
    scrap_resources()
    scrap_recipes()
