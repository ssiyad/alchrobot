# AlchRobot
### Alchemist bot for Chat Wars (Telegram)  

##### Environment Variables
**MODE:** `dev` if using polling else `prod`  
**TOKEN:** Bot token from telegram  
> If heroku (webhook), use MODE=prod  

##### Local setup  
```
git clone https://gitlab.com/ssiyad/alchrobot
cd alchrobot
python3 -m venv venv
source ./venv/bin/activate (.fish)
pip install -r requirements.txt
python bot.py
```
