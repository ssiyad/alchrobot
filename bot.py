import os
import json

from dtypes import Item, ItemList
from telegram import Update
from telegram.ext import (
    Updater,
    MessageHandler,
    Filters,
    CallbackContext,
    CommandHandler,
    InlineQueryHandler,
)

mode = os.environ.get("MODE", "dev")
TOKEN = os.getenv("TOKEN")
if mode == "dev":

    def run(updater: Updater):
        updater.start_polling()


else:

    def run(updater: Updater):
        PORT = int(os.environ.get("PORT", "8443"))
        HEROKU_APP_NAME = os.environ.get("HEROKU_APP_NAME")
        updater.start_webhook(listen="0.0.0.0", port=PORT, url_path=TOKEN)
        updater.bot.set_webhook(
            "https://{}.herokuapp.com/{}".format(HEROKU_APP_NAME, TOKEN)
        )


WELCOME = """
Hello stranger, Nice to meet you!

To start exploring how I can help, I recommend you start with /search ilaves

If you feel I'm missing something, please ping @ssiyad and, if you'd like to contribute to my code, it is available at https://gitlab.com/ssiyad/alchrobot

Once again, Thank you! and, Happy brewing!"""

item_list = ItemList()


def info(update: Update, ctx: CallbackContext):
    update.message.reply_markdown(item_list.get(ctx.match.group(1)).markdown())


def search(update: Update, ctx: CallbackContext):
    update.message.reply_markdown(item_list.markdown(" ".join(ctx.args)))


def inline(update: Update, ctx: CallbackContext):
    update.inline_query.answer(item_list.inline(update.inline_query.query))


def brew(update: Update, ctx: CallbackContext):
    update.message.reply_markdown(item_list.brew(update.message.text))


def start(update: Update, ctx: CallbackContext):
    update.message.reply_markdown(WELCOME, disable_web_page_preview=True)


with open("data.json", "r") as f:
    x = json.load(f)
    for i in x:
        item = Item(x[i])
        item_list.put(item)


updater = Updater(TOKEN)


updater.dispatcher.add_handler(
    MessageHandler(Filters.regex(r"\/i_([a-z|\d]+)"), info, run_async=True)
)
updater.dispatcher.add_handler(
    CommandHandler("search", search, run_async=True, filters=Filters.private)
)
updater.dispatcher.add_handler(InlineQueryHandler(inline))
updater.dispatcher.add_handler(
    MessageHandler(
        Filters.regex(r"(\d+)\ [a-zA-Z|\ |\']+\ (\d+)") & Filters.private,
        brew,
        run_async=True,
    )
)
updater.dispatcher.add_handler(
    CommandHandler("start", start, run_async=True, filters=Filters.private)
)

run(updater)
