import re

from typing import Dict, Union, List
from random import shuffle
from uuid import uuid4
from functools import lru_cache

from telegram import InlineQueryResultArticle, InputTextMessageContent, ParseMode
from telegram.utils.helpers import escape_markdown

EMOJIS = {"yes": "✅", "no": "❌", "mana": "💧"}


class Item:
    def __init__(self, _d: Dict):
        self._id = _d["id"]
        self._name = _d["name"]
        self._list = None
        self._used_in = {}
        if "recipe" in _d:
            self._recipe = _d["recipe"]
            del _d["recipe"]
        else:
            self._recipe = []
        del _d["id"]
        del _d["name"]
        self._attrs = _d

    def get_id(self):
        return self._id

    def get_name(self):
        return self._name

    def set_list(self, item_list):
        self._list = item_list

    def get_attrs(self):
        return self._attrs

    def set_attrs(self, _d: Dict):
        self._attrs = _d

    def add_use(self, _id: str, _count: Union[str, int]):
        self._used_in[_id] = _count

    def get_use(self):
        return self._used_in

    def init_recipe(self):
        for i in self._recipe:
            matches = re.search(r"([a-zA-Z|\ |\']+)\ \((\d+)\)", i)
            _name = matches.group(1)
            _id = matches.group(2)
            x = self._list.get(_id)
            if x:
                if _id not in x.get_use():
                    x.add_use(self.get_id(), self._recipe[i])
            else:
                item = Item(
                    {
                        "id": _id,
                        "name": _name,
                    }
                )
                item.add_use(self.get_id(), self._recipe[i])
                self._list.put(item)

    def get_recipe(self):
        return self._recipe

    @lru_cache(maxsize=128)
    def markdown(self):
        tmp = "*{}* `({})`\n\n".format(
            escape_markdown(self.get_name()), escape_markdown(self.get_id())
        )
        for attr in self._attrs:
            tmp += "*{}:* {}\n".format(
                escape_markdown(attr.capitalize()),
                escape_markdown(
                    self._attrs[attr]
                    .replace("yes", EMOJIS["yes"])
                    .replace("no", EMOJIS["no"])
                ),
            )
        tmp += "\n"

        recipe = self.get_recipe()
        if recipe:
            tmp += "*Recipe:*\n"
            for item in recipe:
                matches = re.search(r"([a-zA-Z|\ |\']+)\ \((\d+)\)", item)
                _name = matches.group(1)
                _id = matches.group(2)
                tmp += "{} {} `{}`\n".format(
                    escape_markdown("/i_" + _id), escape_markdown(_name), recipe[item]
                )
            tmp += "\n"

        uses = self.get_use()
        if uses:
            tmp += "*Used in:*\n"
            for _id in uses:
                item = self._list.get(_id)
                tmp += "{} {}\n".format(
                    item.get_name(), escape_markdown("/i_" + item.get_id())
                )
        return tmp.strip("\n")


class ItemList:
    def __init__(self):
        self._list = {}

    def put(self, item: Item):
        if item.get_id() in self._list:
            self._list[item.get_id()].set_attrs(item.get_attrs())
        else:
            item.set_list(self)
            item.init_recipe()
            self._list[item.get_id()] = item

    def pop(self, item: Item):
        del self._list[item.get_id()]

    def get(self, _id: str) -> Union[Item, None]:
        if _id in self._list:
            return self._list[_id]
        else:
            return None

    @lru_cache(maxsize=128)
    def search(self, keyword: str) -> List:
        tmp = []
        for i in self._list:
            if keyword.lower() in self._list[i].get_name().lower():
                tmp.append(self._list[i])
        shuffle(tmp)
        return tmp

    @lru_cache(maxsize=128)
    def markdown(self, keyword: str) -> str:
        if len(keyword) < 3:
            return "Too short query"
        res = self.search(keyword)
        if len(res) == 1:
            return res[0].markdown()
        tmp = "*Search results:*\n\n"
        if res:
            for item in res:
                tmp += "{} {}\n".format(
                    escape_markdown(item.get_name()),
                    escape_markdown("/i_" + item.get_id()),
                )
            return tmp.strip("\n")
        else:
            return "No results"

    @lru_cache(maxsize=128)
    def inline(self, keyword: str):
        if len(keyword) < 3:
            return []
        return [
            InlineQueryResultArticle(
                id=uuid4(),
                title=i.get_name(),
                input_message_content=InputTextMessageContent(
                    i.markdown(), parse_mode=ParseMode.MARKDOWN
                ),
                description=i.get_id(),
            )
            for i in self.search(keyword)
        ]

    def brew(self, text: str):
        _tmp_d = {}
        _tmp_s = ""
        for herb in re.finditer(r"(p?\d+)\ [a-zA-Z|\ |\']+\ (\d+)", text):
            _id = herb.group(1)
            _count = herb.group(2)
            _item = self.get(_id)
            if not _item:
                print(_id, _count)
                continue
            _use = _item.get_use()
            for i in _use:
                if i in _tmp_d:
                    _tmp_d[i].append(int(_count) // int(_use[i]))
                else:
                    _tmp_d[i] = [int(_count) // int(_use[i])]
        for i in _tmp_d:
            _item = self.get(i)
            if len(_item.get_recipe()) > len(_tmp_d[i]):
                continue
            _max = min(_tmp_d[i])
            if _max < 1:
                continue
            _mana = int(_item.get_attrs()["mana"])
            _tmp_s += "{} {}{}\n".format(
                escape_markdown(_item.get_name()), EMOJIS["mana"], _mana
            )
            for j in [5, 10, 20]:
                if j >= _max:
                    break
                _tmp_s += "[{}](https://t.me/share/url?url=/brew_{} {})   ".format(
                    j, i, j
                )
            _tmp_s += "[{}](https://t.me/share/url?url=/brew_{} {})\n\n".format(
                _max, i, _max
            )
        return _tmp_s.strip("\n")
